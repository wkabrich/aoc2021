
import statistics
import math


def triangle(n: int) -> int:
    return int(n * (n + 1) / 2)


def main():
    with open("./input") as f:
        raw = [ln.strip() for ln in f.readlines()][0].split(',')

    crabs = []
    for crab in raw:
        crabs.append(int(crab))

    part1(crabs)
    part2(crabs)


# Determine the horizontal position that the crabs can align to using the least fuel possible.
# How much fuel must they spend to align to that position?
def part1(crabs: list[int]):
    # best position to move to should just be the median
    midpoint = math.floor(statistics.median(crabs))
    fuel = 0

    for crab in crabs:
        fuel += int(math.fabs(midpoint - crab))

    print(fuel)
    return


def part2(crabs: list[int]):
    crab_max = max(crabs)
    crab_min = min(crabs)
    fuel = triangle(crab_max - crab_min) * len(crabs)    # the most possible fuel

    # iterate over every possible midpoint to find the one that takes the least fuel
    for midpoint in range(crab_min, crab_max):
        temp_fuel = 0
        for crab in crabs:
            # the fuel used by each crab is now the nth triangle number of distance to the midpoint
            temp_fuel += triangle(int(math.fabs(midpoint - crab)))

        if temp_fuel < fuel:
            fuel = temp_fuel

    print(fuel)
    return


if __name__ == "__main__":
    main()
