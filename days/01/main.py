
def main():
    with open("./input") as f:
        raw = [int(ln.strip()) for ln in f.readlines()]

    part1(raw)
    part2(raw)


# How many measurements are larger than the previous measurement?
def part1(raw):
    n = -1
    j = 0

    for i in raw:
        if int(i) > j:
            n += 1
        j = int(i)

    print(n)
    return


# Consider sums of a three-measurement sliding window.
# How many sums are larger than the previous sum?
def part2(raw):
    n = -1
    p = 0

    for i in range(2, len(raw)):
        s = raw[i] + raw[i - 1] + raw[i - 2]
        if s > p:
            n += 1
        p = s

    print(n)
    return


if __name__ == "__main__":
    main()
