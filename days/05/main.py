
def plot_diag_diagram(lines, diagram):
    for line in lines:
        x1, x2 = line[0][0], line[1][0]
        y1, y2 = line[0][1], line[1][1]

        step_x, step_y = 0, 0
        # a diagonal line at exactly 45 degrees.
        if x1 > x2:
            step_x = -1
        elif x1 < x2:
            step_x = 1

        if y1 > y2:
            step_y = -1
        elif y1 < y2:
            step_y = 1

        while x1 != x2 and y1 != y2:
            diagram[y1][x1] += 1
            x1 += step_x
            y1 += step_y
            if x1 == x2 and y1 == y2:
                diagram[y1][x1] += 1


def plot_xy_diagram(lines, diagram):
    for line in lines:
        # horizontal
        if line[0][0] != line[1][0]:
            x1, x2 = line[0][0], line[1][0]
            y = line[0][1]

            if x1 > x2:
                x1, x2 = x2, x1

            for i in range(x1, x2 + 1):
                diagram[y][i] += 1

        # vertical
        elif line[0][1] != line[1][1]:
            y1, y2 = line[0][1], line[1][1]
            x = line[0][0]

            if y1 > y2:
                y1, y2 = y2, y1

            for j in range(y1, y2 + 1):
                diagram[j][x] += 1


def create_diagram(vectors) -> list[list[int]]:
    max_x = 0
    max_y = 0

    for line in vectors:
        if line[0][0] >= max_x:
            max_x = line[0][0] + 1
        if line[1][0] >= max_x:
            max_x = line[1][0] + 1
        if line[0][1] >= max_y:
            max_y = line[0][1] + 1
        if line[1][1] >= max_y:
            max_y = line[1][1] + 1

    return [[0] * max_x for j in range(max_y)]


def count_overlaps(diagram) -> int:
    overlaps = 0

    for j in diagram:
        for i in j:
            if i > 1:
                overlaps += 1

    return overlaps


def main():
    with open("./input") as f:
        raw = [ln.strip() for ln in f.readlines()]

    vectors = []

    for i in raw:
        vector = i.split(' -> ')

        l = vector[0].split(',')
        r = vector[1].split(',')

        vectors.append([[int(l[0]), int(l[1])], [int(r[0]), int(r[1])]])

    part1(vectors.copy())
    part2(vectors.copy())


# determine the number of points where at least two lines overlap.
# At how many points do at least two lines overlap?
def part1(vectors):
    # Consider only horizontal and vertical lines.
    lines = []
    for vector in vectors:
        if vector[0][0] == vector[1][0]:
            lines.append(vector)
        elif vector[0][1] == vector[1][1]:
            lines.append(vector)

    diagram = create_diagram(lines)

    plot_xy_diagram(lines, diagram)
    print(count_overlaps(diagram))
    return


# Consider all of the lines.
# At how many points do at least two lines overlap?
def part2(vectors):
    diagram = create_diagram(vectors)

    straight_lines = []
    diagonal_lines = []

    for vector in vectors:
        if vector[0][0] == vector[1][0]:
            straight_lines.append(vector)
        elif vector[0][1] == vector[1][1]:
            straight_lines.append(vector)
        else:
            diagonal_lines.append(vector)

    plot_xy_diagram(straight_lines, diagram)
    plot_diag_diagram(vectors, diagram)
    print(count_overlaps(diagram))
    return


if __name__ == "__main__":
    main()
