
def main():
    with open("./input") as f:
        raw = [ln.strip() for ln in f.readlines()]

    instructions = []

    for i in raw:
        instructions.append(i.split(' '))

    part1(instructions)
    part2(instructions)


# What do you get if you multiply your final horizontal position by your final depth?
def part1(instructions):
    x = 0
    y = 0

    for i in instructions:
        if i[0] == 'forward':
            x += int(i[1])
        elif i[0] == 'down':
            y += int(i[1])
        elif i[0] == 'up':
            y -= int(i[1])

    print(x * y)
    return


# What do you get if you multiply your final horizontal position by your final depth?
def part2(instructions):
    aim = 0
    x = 0
    y = 0

    for i in instructions:
        if i[0] == 'forward':
            x += int(i[1])
            y += aim * int(i[1])
        elif i[0] == 'down':
            aim += int(i[1])
        elif i[0] == 'up':
            aim -= int(i[1])

    print(x * y)
    return


if __name__ == "__main__":
    main()
