
def main():
    with open("./input") as f:
        raw = [ln.strip() for ln in f.readlines()]

    part1(raw)
    part2(raw)


def part1(raw):
    return


def part2(raw):
    return


if __name__ == "__main__":
    main()
