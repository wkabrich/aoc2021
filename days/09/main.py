
import math


def get_basin_size(i: int, j: int, heightmap: list[list[int]]) -> int:
    size = 0
    floor = (len(heightmap), len(heightmap[0]))

    neighbors = [
        (i - 1, j),
        (i + 1, j),
        (i, j + 1),
        (i, j - 1)
    ]

    # count the current position towards basin size and mark it on the heightmap
    heightmap[i][j] = -1
    size += 1

    # get basin size of the neighbors
    for x, y in neighbors:
        if 0 <= x < floor[0] \
                and 0 <= y < floor[1] \
                and heightmap[x][y] not in (-1, 9):
            size += get_basin_size(x, y, heightmap)

    return size


def is_low_point(i: int, j: int, heightmap: list[list[int]]) -> bool:
    low = False
    higher_neighbors = 0

    try:
        if heightmap[i][j] < heightmap[i - 1][j]:
            higher_neighbors += 1
    except IndexError:
        higher_neighbors += 1

    try:
        if heightmap[i][j] < heightmap[i + 1][j]:
            higher_neighbors += 1
    except IndexError:
        higher_neighbors += 1

    try:
        if heightmap[i][j] < heightmap[i][j + 1]:
            higher_neighbors += 1
    except IndexError:
        higher_neighbors += 1

    try:
        if heightmap[i][j] < heightmap[i][j - 1]:
            higher_neighbors += 1
    except IndexError:
        higher_neighbors += 1

    if higher_neighbors == 4:
        low = True

    return low


def main():
    with open("./input") as f:
        raw = [ln.strip() for ln in f.readlines()]

    heightmap = []
    for r in raw:
        t = list(r)
        for i in range(len(t)):
            t[i] = int(t[i])
        heightmap.append(t)

    part1(heightmap)
    part2(heightmap)


def part1(heightmap):
    risk_level = 0

    for i in range(len(heightmap)):
        for j in range(len(heightmap[i])):
            if is_low_point(i, j, heightmap):
                risk_level += 1 + heightmap[i][j]

    print(risk_level)
    return


def part2(heightmap):
    basin_sizes = []

    for i in range(len(heightmap)):
        for j in range(len(heightmap[i])):
            if is_low_point(i, j, heightmap):
                basin_sizes.append(get_basin_size(i, j, heightmap.copy()))

    print(math.prod(sorted(basin_sizes, reverse=True)[:3]))
    return


if __name__ == "__main__":
    main()
