

class Octopus:
    charge: int
    flashing: bool

    def __init__(self, charge: int):
        self.charge = int(charge)
        self.flashing = False

    def add_charge(self):
        self.charge += 1

    def try_flash(self) -> bool:
        if self.charge > 9 and self.flashing:
            self.charge = 0
            self.flashing = False
            return True
        else:
            return False


def spread_flash(consortium: list[list[Octopus]]):
    flashing = 1
    size_x = len(consortium)
    size_y = len(consortium[0])
    neighbors = {(-1, -1), (-1, 0), (-1, 1),
                 (0, -1), (0, 1),
                 (1, -1), (1, 0), (1, 1)}

    while flashing >= 1:
        temp_flashing = 0
        for i in range(len(consortium)):
            for j in range(len(consortium[i])):
                if consortium[i][j].charge > 9:
                    if not consortium[i][j].flashing:
                        consortium[i][j].flashing = True
                        temp_flashing += 1
                        for dx, dy in neighbors:
                            x, y = i + dx, j + dy
                            if 0 <= x < size_x and 0 <= y < size_y:
                                consortium[x][y].charge += 1
        flashing = temp_flashing

    return


def flash(consortium: list[list[Octopus]]) -> int:
    flashes = 0

    for ln in consortium:
        for o in ln:
            flashed = o.try_flash()
            if flashed:
                flashes += 1

    return flashes


def main():
    with open("./input") as f:
        raw = [list(ln.strip()) for ln in f.readlines()]

    consortium = [[Octopus(int(o)) for o in ln] for ln in raw]

    part1(consortium)
    part2(consortium)


def part1(consortium):
    flashes = 0
    steps = 100

    for s in range(steps):
        for ln in consortium:
            for o in ln:
                o.add_charge()
        spread_flash(consortium)
        flashes += flash(consortium)

    print(flashes)
    return


def part2(consortium):
    flashes = 0
    consortium_size = len(consortium) * len(consortium[0])
    steps = 100

    while flashes != consortium_size:
        steps += 1
        for ln in consortium:
            for o in ln:
                o.add_charge()
        spread_flash(consortium)
        flashes = flash(consortium)

    print(steps)
    return


if __name__ == "__main__":
    main()
