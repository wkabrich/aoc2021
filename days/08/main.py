
def get_key(d: dict, v: any) -> any:
    for key, value in d.items():
        if value == v:
            return key


def decode(signals: list[str]) -> dict:
    working_signals = set(signals)
    pattern = {}

    # known lengths
    for signal in working_signals.copy():
        # 1
        if len(signal) == 2:
            pattern[''.join(sorted(signal))] = '1'
            working_signals.remove(signal)
        # 4
        if len(signal) == 4:
            pattern[''.join(sorted(signal))] = '4'
            working_signals.remove(signal)
        # 7
        if len(signal) == 3:
            pattern[''.join(sorted(signal))] = '7'
            working_signals.remove(signal)
        # 8
        if len(signal) == 7:
            pattern[''.join(sorted(signal))] = '8'
            working_signals.remove(signal)

    # 3 is the only pattern of length 5 that entirely contains 1
    for signal in working_signals.copy():
        if len(signal) == 5:
            if set(signal).issuperset(set(get_key(pattern, '1'))):
                pattern[''.join(sorted(signal))] = '3'
                working_signals.remove(signal)

    # 9 is the only pattern of length 6 that entirely contains 4
    for signal in working_signals.copy():
        if len(signal) == 6:
            if set(signal).issuperset(set(get_key(pattern, '4'))):
                pattern[''.join(sorted(signal))] = '9'
                working_signals.remove(signal)

    # 2 is the only pattern of length 5 that contains 8 difference 9
    for signal in working_signals.copy():
        if len(signal) == 5:
            if set(signal).issuperset(set(get_key(pattern, '8')).difference(set(get_key(pattern, '9')))):
                pattern[''.join(sorted(signal))] = '2'
                working_signals.remove(signal)

    # 5 is the only pattern of length 5 left
    for signal in working_signals.copy():
        if len(signal) == 5:
            pattern[''.join(sorted(signal))] = '5'
            working_signals.remove(signal)

    # 6 is the only pattern of length 6 left that entirely contains 5
    for signal in working_signals.copy():
        if len(signal) == 6:
            if set(signal).issuperset(set(get_key(pattern, '5'))):
                pattern[''.join(sorted(signal))] = '6'
                working_signals.remove(signal)

    # 0 is the only pattern left
    pattern[''.join(sorted(working_signals.pop()))] = '0'

    return pattern


def main():
    with open("./input") as f:
        raw = [ln.strip() for ln in f.readlines()]

    notes = []
    for note in raw:
        note = note.split(' | ')
        note[0] = note[0].split(' ')
        note[1] = note[1].split(' ')
        notes.append(note)

    part1(notes)
    part2(notes)


def part1(notes):
    count = 0
    for note in notes:
        for output in note[1]:
            if len(output) in (2, 3, 4, 7):
                count += 1

    print(count)
    return


def part2(notes):
    decoded_outputs = []

    for note in notes:
        decoded = ''
        pattern = decode(note[0])
        for val in note[1]:
            decoded += pattern[''.join(sorted(val))]

        decoded_outputs.append(int(decoded))

    print(sum(decoded_outputs))
    return


if __name__ == "__main__":
    main()
