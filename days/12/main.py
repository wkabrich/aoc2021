

class CaveNetwork:
    graph: dict
    small: list[str]
    all_paths: set
    all_paths_two: set

    def __init__(self, graph: dict):
        self.graph = graph
        self.small = [x for x in list(graph.keys()) if x.islower()]
        self.all_paths = set()
        self.all_paths_two = set()

    def get_paths_to(self, u, d, visited, path=None):
        if path is None:
            path = ''

        if u in self.small:
            visited[u] = True

        path += u + ','

        if u == d:
            self.all_paths.add(path)
        else:
            for n in self.graph[u]:
                if (n in self.small and not visited[n]) or n not in self.small:
                    self.get_paths_to(n, d, visited, path)

        visited[u] = False

    def get_paths_two(self, u, d, path=None):
        if path is None:
            path = ''

        path += u + ','

        if u == d:
            self.all_paths_two.add(path)
        else:
            small_twice = False
            for i in self.small:
                if path.split(',').count(i) == 2:
                    small_twice = True
            for n in self.graph[u]:
                in_path = path.split(',').count(n)
                if n in self.small:
                    if small_twice and in_path < 1:
                        self.get_paths_two(n, d, path)
                    elif not small_twice and in_path < 2 and n not in ['start', 'end']:
                        self.get_paths_two(n, d, path)
                    elif not small_twice and in_path < 1 and n in ['start', 'end']:
                        self.get_paths_two(n, d, path)
                else:
                    self.get_paths_two(n, d, path)

    def get_all_paths(self, s, d):
        visited = {x: False for x in list(self.graph.keys())}
        self.get_paths_to(s, d, visited)

    def get_all_paths_two(self, s, d):
        self.get_paths_two(s, d)


def main():
    with open("./input") as f:
        raw = [ln.strip() for ln in f.readlines()]

    graph = {}
    for link in raw:
        node, edge = link.split('-')
        if node not in graph:
            graph[node] = {edge}
        elif edge not in graph[node]:
            graph[node].add(edge)

        # graph is not directional. create the reverse link
        if edge not in graph:
            graph[edge] = {node}
        elif node not in graph[edge]:
            graph[edge].add(node)

    part1(graph)
    part2(graph)


# How many paths through this cave system are there that visit small caves at most once?
def part1(graph):
    cn = CaveNetwork(graph)
    cn.get_all_paths('start', 'end')
    print(len(cn.all_paths))
    return


def part2(graph):
    cn = CaveNetwork(graph)
    cn.get_all_paths_two('start', 'end')
    print(len(cn.all_paths_two))
    return


if __name__ == "__main__":
    main()
