
def main():
    with open("./input") as f:
        raw = [ln.strip() for ln in f.readlines()]

    part1(raw)
    part2(raw)


def part1(lines):
    openers = {'(', '[', '{', '<'}
    closers = {')', ']', '}', '>'}
    close_opens = {')': '(', ']': '[', '}': '{', '>': '<'}
    points = {')': 3, ']': 57, '}': 1197, '>': 25137}
    score = 0

    for line in lines:
        opens = []
        for c in line:
            if c in openers:
                opens.append(c)
            elif c in closers:
                if close_opens[c] == opens[-1]:
                    opens = opens[:-1]
                else:
                    score += points[c]
                    break

    print(score)
    return


def part2(lines):
    openers = {'(', '[', '{', '<'}
    closers = {')', ']', '}', '>'}
    close_opens = {')': '(', ']': '[', '}': '{', '>': '<'}
    open_closes = {'(': ')', '[': ']', '{': '}', '<': '>'}
    points = {')': 1, ']': 2, '}': 3, '>': 4}
    scores = []

    for line in lines:
        opens = []
        is_incomplete = True
        for c in line:
            if c in openers:
                opens.append(c)
            elif c in closers:
                if close_opens[c] == opens[-1]:
                    opens = opens[:-1]
                else:
                    is_incomplete = False
                    break

        if is_incomplete:
            sub_score = 0
            opens.reverse()
            for o in opens:
                sub_score = sub_score * 5 + points[open_closes[o]]
            scores.append(sub_score)

    print(sorted(scores)[int(len(scores) / 2)])
    return


if __name__ == "__main__":
    main()
