
def main():
    with open("./input") as f:
        raw = [ln.strip() for ln in f.readlines()]

    part1(raw)
    part2(raw)


# Use the binary numbers in your diagnostic report to calculate the gamma rate and epsilon rate,
# then multiply them together.
# What is the power consumption of the submarine?
def part1(raw):
    most = []
    least = []

    for pos in range(12):
        zeroes = 0
        ones = 0

        for n in raw:
            if int(n[pos]) == 0:
                zeroes += 1
            elif int(n[pos]) == 1:
                ones += 1

        if zeroes > ones:
            most.append('0')
            least.append('1')
        elif ones > zeroes:
            most.append('1')
            least.append('0')

    gamma = int(''.join(most), 2)
    epsilon = int(''.join(least), 2)

    print(gamma * epsilon)

    return


# Use the binary numbers in your diagnostic report to calculate the oxygen generator rating and CO2 scrubber rating,
# then multiply them together.
# What is the life support rating of the submarine?
def part2(raw):
    oxygen_rating = int(get_rating(raw, 1), 2)
    c02_rating = int(get_rating(raw, 0), 2)

    print(oxygen_rating * c02_rating)

    return


def find_most_and_least_common_bit(report: list[str], place: int) -> list[int]:
    zeroes = 0
    ones = 0
    most_common = 1
    least_common = 0

    for n in report:
        bit = n[place]
        if bit == '0':
            zeroes += 1
        elif bit == '1':
            ones += 1

    if zeroes > ones:
        most_common = 0
        least_common = 1

    return [least_common, most_common]


def filter_report(report: list[str], bit: int, place: int) -> list[str]:
    filtered_report = []

    for n in report:
        if int(n[place]) == bit:
            filtered_report.append(n)

    return filtered_report


def get_rating(report: list[str], target_bit: int, place=0) -> str:
    # base case
    if len(report) == 1:
        return report[0]

    first_bits = find_most_and_least_common_bit(report, place)
    filtered_report = filter_report(report, first_bits[target_bit], place)

    return get_rating(filtered_report, target_bit, place=place + 1)


if __name__ == "__main__":
    main()
