
def main():
    with open("./input") as f:
        raw = [ln.strip() for ln in f.readlines()]

    raw = raw[0].split(',')
    school = []
    for fish in raw:
        school.append(int(fish))

    part1(school.copy())
    part2(school.copy())


# How many lanternfish would there be after 80 days?
def part1(school):
    days = 80

    for day in range(days):
        new_fish = []
        for i in range(len(school)):
            school[i] -= 1
            if school[i] < 0:
                school[i] = 6
                new_fish.append(8)

        school += new_fish

    print(len(school))
    return


# How many lanternfish would there be after 256 days?
def part2(school):
    days = 256

    # Instead of using lantern fish, would grouper fish have been too big of a clue?
    schools = {0: 0,
               1: 0,
               2: 0,
               3: 0,
               4: 0,
               5: 0,
               6: 0,
               7: 0,
               8: 0,
               }

    # separate the fish from one school into many based on time to spawn
    for fish in school:
        schools[fish] += 1

    for day in range(days):
        # each school moves closer to spawning by one day.
        # day 6 school is day 7's school plus the fish spawning in day 0
        # day 8 school is the number of new fish spawned
        schools[0], schools[1], schools[2], schools[3], schools[4], schools[5], schools[6], schools[7], schools[8] \
            = schools[1], schools[2], schools[3], schools[4], schools[5], schools[6], \
            schools[7] + schools[0], schools[8], schools[0]

    print(sum(schools.values()))
    return


if __name__ == "__main__":
    main()
