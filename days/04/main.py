
class BingoCard:
    card: list[list[int]]
    markers: list[list[bool]]
    has_won: bool

    def __init__(self, card: list[list[int]]):
        self.card = card
        self.markers = []
        self.has_won = False

        for row in self.card:
            marker_row = []
            for square in row:
                marker_row.append(False)
            self.markers.append(marker_row)

    def place_marker(self, ball: int) -> bool:
        has_number = False

        for i in range(len(self.card)):
            for j in range(len(self.card[i])):
                if self.card[i][j] == ball:
                    has_number = True
                    self.markers[i][j] = True

        return has_number

    def check_if_winner(self) -> bool:
        # check horizontal for win
        for row in self.markers:
            # Wins until proven false
            winner = True
            for square in row:
                winner &= square
            if winner:
                self.has_won = True
                break

        # check vertical for the win
        for j in range(len(self.card[0])):
            # Wins until proven false
            winner = True
            for row in self.markers:
                winner &= row[j]
            if winner:
                self.has_won = True
                break

        return self.has_won

    def unmarked_sum(self) -> int:
        unmarked_sum = 0

        for i in range(len(self.card)):
            for j in range(len(self.card[i])):
                if not self.markers[i][j]:
                    unmarked_sum += self.card[i][j]

        return unmarked_sum


def prep_game(raw) -> (list[int], list[BingoCard]):
    balls = []
    cards = []

    unprocessed_balls = raw[0].split(',')
    unprocessed_cards = raw[2:]
    temp_card = []

    for ball in unprocessed_balls:
        balls.append(int(ball))

    for row in unprocessed_cards:
        temp_row = []
        if row == '':
            cards.append(BingoCard(temp_card))
            temp_card = []
        else:
            # there is surely a better way, but I'm on an airplane and can't remember
            for n in row.split(' '):
                if n != '':
                    temp_row.append(int(n))
            temp_card.append(temp_row)

    return balls, cards


def main():
    with open("./input") as f:
        raw = [ln.strip() for ln in f.readlines()]

    game = prep_game(raw)

    cards = game[1]
    balls = game[0]

    part1(cards.copy(), balls.copy())
    part2(cards.copy(), balls.copy())


# Start by finding the sum of all unmarked numbers on that board;
# Then, multiply that sum by the number that was just called when the board won
# What will your final score be if you choose that board?
def part1(cards: list[BingoCard], balls: list[int]):
    winning_ball = None
    winning_card = None

    for n in balls:
        if winning_card is None:
            for card in cards:
                card.place_marker(n)
                if card.check_if_winner():
                    winning_ball = n
                    winning_card = card
                    break

    winning_sum = winning_card.unmarked_sum()
    winning_score = winning_sum * winning_ball
    print(winning_score)
    return


def part2(cards: list[BingoCard], balls: list[int]):
    last_winning_ball = None
    last_winning_card = None
    cards_in_play = cards.copy()

    for n in balls:
        cards_not_winning = []
        for card in cards_in_play:
            card.place_marker(n)
            if card.check_if_winner():
                last_winning_ball = n
                last_winning_card = card
            else:
                cards_not_winning.append(card)
        cards_in_play = cards_not_winning.copy()

    last_winning_sum = last_winning_card.unmarked_sum()
    last_winning_score = last_winning_sum * last_winning_ball
    print(last_winning_score)
    return


if __name__ == "__main__":
    main()
